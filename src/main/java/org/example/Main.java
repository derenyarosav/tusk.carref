package org.example;

public class Main {
    public static void main(String[] args) {
        Lorry Ford = new Lorry();
        SportCar Ferrari = new SportCar();
        Ford.setLiftingCapacity(4000);
        Ford.setaClass("Lorry");
        Ford.setProducer("Genri Ford");
        Ford.setWeight(5000);
        Ferrari.setSpeed(350);
        Ferrari.setaClass("Sport car");
        Ferrari.setProducer("Enzo Ferrari");
        Ferrari.setWeight(500);
        System.out.println(Ford.getLiftingCapacity());
        System.out.println(Ford.getClass());
        System.out.println(Ford.getProducer());
        System.out.println(Ford.getWeight());
        System.out.println(Ferrari.getSpeed());
        System.out.println(Ferrari.getaClass());
        System.out.println(Ferrari.getProducer());
        System.out.println(Ferrari.getWeight());
        Ford.start();
        Ford.stop();
        Ford.openTrunk();
        Ford.closeTrunk();
        Ford.SportDrivingMode();
        Ford.urbanDrivingMode();
        Ford.leftTurnSignal();
        Ford.rightTurnSignal();
        Ford.turnOffTheBacklight();
        Ford.turnOnTheBacklight();
        Ford.turnOffTheHeadlight();
        Ford.turnOnTheHeadlight();
        Ferrari.start();
        Ferrari.stop();
        Ferrari.openTrunk();
        Ferrari.closeTrunk();
        Ferrari.SportDrivingMode();
        Ferrari.urbanDrivingMode();
        Ferrari.leftTurnSignal();
        Ferrari.rightTurnSignal();
        Ferrari.turnOffTheBacklight();
        Ferrari.turnOnTheBacklight();
        Ferrari.turnOffTheHeadlight();
        Ferrari.turnOnTheHeadlight();

    }
}