package org.example;

public class Car {
    private String producer;
    private String aClass;
    private double weight;


    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getaClass() {
        return aClass;
    }

    public void setaClass(String aClass) {
        this.aClass = aClass;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


    public void start() {
        System.out.println("Поехали");
    }

    public void stop() {
        System.out.println("Останавливаемся");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот налево");
    }
    public void turnOnTheHeadlight() {
        System.out.println("turn on the headlight");
    }
    public void turnOffTheHeadlight() {
        System.out.println("turn off the headlight");
    }
    public void rightTurnSignal() {
        System.out.println("Left turn signal");
    }
    public void leftTurnSignal() {
        System.out.println("Right turn signal");
    }
    public void turnOnTheBacklight() {
        System.out.println("Turn on the backlight");
    }
    public void turnOffTheBacklight() {
        System.out.println("Turn off the backlight");
    }
    public void openTrunk() {
        System.out.println("Open trunk");
    }
    public void closeTrunk() {
        System.out.println("Close trunk");
    }
    public void SportDrivingMode() {
        System.out.println("Spots mode is on");
    }
    public void urbanDrivingMode() {
        System.out.println("Urban mode is on");
    }



    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", aClass='" + aClass + '\'' +
                ", weight=" + weight +
                '}';
    }
}
